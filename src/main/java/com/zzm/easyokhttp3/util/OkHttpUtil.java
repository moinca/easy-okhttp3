package com.zzm.easyokhttp3.util;

import com.zzm.easyokhttp3.http.Client;
import com.zzm.easyokhttp3.http.HttpConfiguration;

/**
 * @ClassName OkHttpUtil
 * @Description 工具类
 * @Author zzm
 * @Date 2019/6/12
 * @Version 1.0
 */
public class OkHttpUtil {
	private static Client client = new Client(new HttpConfiguration());

	/**
	 * 提供默认的客户端
	 *
	 * @return
	 */
	public static Client getClient() {
		return client;
	}
}
