package com.zzm.easyokhttp3.http;



public class HttpConfiguration implements Cloneable {
	/**
	 * 连接超时时间 单位秒(默认10s)
	 */
	public int connectTimeout = 10;
	/**
	 * 写超时时间 单位秒(默认 0 , 不超时)
	 */
	public int writeTimeout = 0;
	/**
	 * 回复超时时间 单位秒(默认30s)
	 */
	public int readTimeout = 30;
	/**
	 * 底层HTTP库所有的并发执行的请求数量
	 */
	public int dispatcherMaxRequests = 64;
	/**
	 * 底层HTTP库对每个独立的Host进行并发请求的数量
	 */
	public int dispatcherMaxRequestsPerHost = 32;
	/**
	 * 底层HTTP库中复用连接对象的最大空闲数量
	 */
	public int connectionPoolMaxIdleCount = 32;
	/**
	 * 底层HTTP库中复用连接对象的回收周期（单位分钟）
	 */
	public int connectionPoolMaxIdleMinutes = 2;
}
