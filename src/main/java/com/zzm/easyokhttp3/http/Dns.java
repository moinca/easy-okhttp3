package com.zzm.easyokhttp3.http;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
/**
 * @ClassName Dns
 * @Description Dns 配置
 * @Author zzm
 * @Date 2019/6/13
 * @Version 1.0
 */
public interface Dns {
    List<InetAddress> lookup(String hostname) throws UnknownHostException;
}
