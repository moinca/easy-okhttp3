package com.zzm.easyokhttp3.http;

/**
 * 请求处理完成的异步回调接口
 */
public interface AsyncCallback {
	/**
	 * 异步回调
	 *
	 * @param response
	 */
	void complete(Response response);
}
