package com.zzm;

import com.zzm.easyokhttp3.http.Client;
import com.zzm.easyokhttp3.http.HttpConfiguration;
import com.zzm.easyokhttp3.http.Response;
import com.zzm.easyokhttp3.util.OkHttpUtil;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

/**
 * @ClassName Test
 * @Description test
 * @Author cjh
 * @Date 2019/6/13 16:42
 * @Version 1.0
 */
public class Test {

    @org.junit.Test
    public void connectSuccess() {
        Response response = OkHttpUtil.getClient().get("https://www.baidu.com/?tn=00013449_oem_dg");
        if (response.isResponseOK()) {
            System.out.println(response.getBodyString());
        } else {
            System.out.println(response.toString());
        }
    }

    static ExecutorService executorService = Executors.newFixedThreadPool(4);

    private static Client client;
    private static Client client1;

    static {
        HttpConfiguration httpConfiguration = new HttpConfiguration();
        httpConfiguration.connectTimeout = 30;
        httpConfiguration.readTimeout = 30;
        httpConfiguration.dispatcherMaxRequests = 256;

        httpConfiguration.dispatcherMaxRequestsPerHost = 256;

        httpConfiguration.connectionPoolMaxIdleCount = 256;
        client = new Client(httpConfiguration);
        client1 = new Client(httpConfiguration);
    }
}
